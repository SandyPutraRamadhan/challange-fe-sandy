import './App.css';
import { Route, Routes } from 'react-router-dom';
import Login from './Component/Login.jsx';
import Register from './Component/Register';
import Home from './Pages/Home';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/home' element={<Home/>}/>
      </Routes>
    </div>
  );
}

export default App;
